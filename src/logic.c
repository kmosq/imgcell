
void start_region(struct State *state) {
	state->region_active = 1;
	Vector inpos = tvti(state, state->m_pos);
	state->region[0] = inpos;
	state->region[1] = inpos;
	state->redraw_needed = 1;
}

void end_region(struct State *state) {
}


void print_fmt(struct State *state, FILE *file, char *fmt) {
	Vector min;
	Vector max;
	max.x = -min2(state->region[0].x, state->region[1].x) + (state->image_size.x / 2);
	max.y = -min2(state->region[0].y, state->region[1].y) + (state->image_size.y / 2);
	min.x = -max2(state->region[0].x, state->region[1].x) + (state->image_size.x / 2);
	min.y = -max2(state->region[0].y, state->region[1].y) + (state->image_size.y / 2);

	max.x = clamp(max.x, 0, state->image_size.x);
	max.y = clamp(max.y, 0, state->image_size.y);
	min.x = clamp(min.x, 0, state->image_size.x);
	min.y = clamp(min.y, 0, state->image_size.y);

	Vector size;
	size.x = max.x - min.x;
	size.y = max.y - min.y;

	int st = 0;
	while (*fmt) {
		if (st == 0) {
			if (*fmt == '\\') {
				st = 2;
			} else if (*fmt == '%') {
				st = 1;
			} else {
				fputc(*fmt, file);
			}
		} else if (st == 2) {
			fputc(*fmt, file);
			st = 0;
		} else if (st == 1) {
			switch (*fmt) {
				case 'x':
					fprintf(file, "%i", min.x);
					break;
				case 'X':
					fprintf(file, "%i", max.x);
					break;
				case 'y':
					fprintf(file, "%i", min.y);
					break;
				case 'Y':
					fprintf(file, "%i", max.y);
					break;
				case 'w':
					fprintf(file, "%i", size.x);
					break;
				case 'h':
					fprintf(file, "%i", size.y);
					break;
			}
			st = 0;
		}

		fmt++;
	}

	fprintf(file, "\n");

}

void update_region(struct State *state) {
	if (state->lm_down == 1) {
		state->region[1] = tvti(state, state->m_pos);
	}
	state->redraw_needed = 1;
	
}

void quit(struct State *state) {
	if (state->region_active) {
		print_fmt(state, stdout, state->fmt_string);
	}

	state->running = 0;
}
