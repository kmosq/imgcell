struct State {
	SDL_Window *window;
	SDL_Renderer *renderer;
	Vector window_size;

	int running;


	Vector m_pos;
	int lm_down;
	int rm_down;
	int mm_down;

	int redraw_needed;

	SDL_Surface *image;
	SDL_Texture *image_texture;
	Vector image_size;

	Vector viewport_pos;
	double zoom;

	int region_active;
	Vector region[2];

	char *fmt_string;
};

void init_state(struct State *state) {
	state->window = NULL;
	state->renderer = NULL;
	state->window_size = (Vector){0, 0};

	state->running = 1;

	state->m_pos = (Vector){0, 0};
	state->lm_down = 0;
	state->rm_down = 0;
	state->mm_down = 0;

	state->redraw_needed = 1;

	state->viewport_pos = (Vector){0, 0};
	state->zoom = 1.0;

	state->region_active = 0;

	state->fmt_string = "%x:%y:%w:%h\n";
}
