// translate vector to window

SDL_BlendMode inverse_blend_mode;

void draw_statusbar(struct State *state) {
	const int size = 20;
	SDL_Rect r;
	r.x = 0;
	r.y = state->window_size.y - size;
	r.w = state->window_size.x;
	r.h = size;
	SDL_SetRenderDrawColor(state->renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(state->renderer, &r);
	SDL_SetRenderDrawColor(state->renderer, 255, 255, 255, 255);
	SDL_RenderDrawLine(state->renderer, 0, state->window_size.y - size, state->window_size.x, state->window_size.y - size);
}

void draw_region(struct State *state) {
	if (!state->region_active) {
		return;
	}

	Vector region[2];
	region[0] = tvtw(state, state->region[0]);
	region[1] = tvtw(state, state->region[1]);

	SDL_Point points[5] = {
		(SDL_Point){region[0].x, region[0].y},
		(SDL_Point){region[1].x, region[0].y},
		(SDL_Point){region[1].x, region[1].y},
		(SDL_Point){region[0].x, region[1].y},
		(SDL_Point){region[0].x, region[0].y}
	};
	
	SDL_SetRenderDrawColor(state->renderer, 255, 255, 255, 255);
	SDL_RenderDrawLines(state->renderer, points, 5);
}

void draw_image(struct State *state) {
	SDL_Rect target;

	Vector is = state->image_size;
	Vector wp = state->viewport_pos;
	Vector ws = state->window_size;

	Vector tpos = tvtw(state, (Vector){is.x / 2, is.y / 2});
	
	target.x = tpos.x;
	target.y = tpos.y;
	target.w = is.x * state->zoom;
	target.h = is.y * state->zoom;

	SDL_RenderCopy(state->renderer, state->image_texture, NULL, &target);

}

void redraw(struct State *state) {
	SDL_SetRenderDrawColor(state->renderer, 74, 86, 107, 255);
	SDL_RenderClear(state->renderer);
	draw_image(state);
	draw_region(state);
	//draw_statusbar(state);
	SDL_RenderPresent(state->renderer);
}

void ui_init(struct State *state) {
}

void ui_destroy(struct State *state) {
}
