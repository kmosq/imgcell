void handle_button(struct State *state, SDL_MouseButtonEvent *button) {
	int *button_state = NULL;

	switch (button->button) {
		case SDL_BUTTON_LEFT:
			button_state = &state->lm_down;
			break;
		case SDL_BUTTON_RIGHT:
			button_state = &state->rm_down;
			break;
		case SDL_BUTTON_MIDDLE:
			button_state = &state->mm_down;
			break;
	}

	if (button_state == NULL) {
		return;
	}

	switch (button->state) {
		case SDL_PRESSED:
			*button_state = 1;
			break;
		case SDL_RELEASED:
			*button_state = 0;
			break;
	}

	state->m_pos = (Vector){button->x, button->y};

	if (button->button == SDL_BUTTON_LEFT) {
		switch (button->state) {
			case SDL_PRESSED:
				start_region(state);
				break;
			case SDL_RELEASED:
				end_region(state);
				break;
		}
	}
}

void handle_key(struct State *state, SDL_KeyboardEvent *key) {
	if (key->state != SDL_PRESSED) {
		return;
	}

	int po = (int)ceil(20.0 / state->zoom);

	switch (key->keysym.sym) {
		case SDLK_h:
			state->viewport_pos.x += po;
			state->redraw_needed = 1;
			break;
		case SDLK_j:
			state->viewport_pos.y -= po;
			state->redraw_needed = 1;
			break;
		case SDLK_k:
			state->viewport_pos.y += po;
			state->redraw_needed = 1;
			break;
		case SDLK_l:
			state->viewport_pos.x -= po;
			state->redraw_needed = 1;
			break;
		case SDLK_p:
			state->zoom -= 0.02;
			state->redraw_needed = 1;
			break;
		case SDLK_n:
			state->zoom += 0.02;
			state->redraw_needed = 1;
			break;
		case SDLK_q:
			quit(state);
			break;
		case SDLK_r:
			state->region_active = 0;
			state->redraw_needed = 1;
			break;
	}

}

void handle_motion(struct State *state, SDL_MouseMotionEvent *motion) {
	Vector new_pos = (Vector){motion->x, motion->y};
	Vector rel = (Vector){new_pos.x - state->m_pos.x, new_pos.y - state->m_pos.y};
	state->m_pos = new_pos;
	
	if (rel.x != 0 || rel.y != 0) {
		if (state->lm_down) {
			update_region(state);
			return;
		}

		if (state->mm_down && !state->lm_down) {
			state->viewport_pos.x -= rel.x / state->zoom;
			state->viewport_pos.y -= rel.y / state->zoom;
			state->redraw_needed = 1;
			return;
		}
	}
}

void handle_wheel(struct State *state, SDL_MouseWheelEvent *wheel) {
	state->zoom += 0.02 * wheel->y;
	state->redraw_needed = 1;
}

