void handle_window_event(struct State *state, SDL_WindowEvent *event) {
	switch (event->event) {
		case SDL_WINDOWEVENT_SIZE_CHANGED:
			state->window_size = (Vector){event->data1, event->data2};
			break;
	}

	state->redraw_needed = 1;
}

void handle_event(struct State *state, SDL_Event *event) {
	switch (event->type) {
		case SDL_QUIT:
			state->running = 0;	
			break;
		case SDL_MOUSEBUTTONDOWN:
			handle_button(state, &event->button);
			break;
		case SDL_MOUSEBUTTONUP:
			handle_button(state, &event->button);
			break;
		case SDL_MOUSEMOTION:
			handle_motion(state, &event->motion);
			break;
		case SDL_MOUSEWHEEL:
			handle_wheel(state, &event->wheel);
			break;
		case SDL_KEYUP:
			handle_key(state, &event->key);
			break;
		case SDL_KEYDOWN:
			handle_key(state, &event->key);
			break;
		case SDL_WINDOWEVENT:
			handle_window_event(state, &event->window);
			break;
	}
}
