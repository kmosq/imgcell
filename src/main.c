#include <stdio.h>
#include <math.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

typedef struct {
	int x;
	int y;
} Vector;

#include "state.c"
#include "util.c"
#include "logic.c"
#include "ui.c"
#include "input.c"
#include "event.c"


void print_help() {
	printf("Usage: imgcell (image path) (format string)\n");
}

int main(int argc, char **argv) {
	srand(time(0));

	if (argc < 2) {
		fprintf(stderr, "No image path\n");
		print_help();
		return 1;
	}

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		fprintf(stderr, "Failed to initialize SDL\n");
		return 1;
	}
	
	int flags = IMG_INIT_PNG | IMG_INIT_JPG;
	int img_init = IMG_Init(flags);

	if (flags != img_init) {
		fprintf(stderr, "Failed to initialize IMG\n");
		return 1;
	}

	SDL_Surface *image = IMG_Load(argv[1]);
	if (image == NULL) {
		fprintf(stderr, "Failed to load image %s\n", argv[1]);
		return 1;
	}

	SDL_Window *window;
	SDL_Renderer *renderer;

	window = SDL_CreateWindow("imgcell", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 500, 500, SDL_WINDOW_RESIZABLE);
	renderer = SDL_CreateRenderer(window, -1, 0);

	struct State state;
	init_state(&state);

	if (argc > 2) {
		state.fmt_string = argv[2];
	}

	state.window = window;
	state.renderer = renderer;
	
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, image);

	state.image = image;
	state.image_texture = texture;
	state.image_size = (Vector){image->w, image->h};

	SDL_GetWindowSize(window, &state.window_size.x, &state.window_size.y);
	
	SDL_Event event;
	while (state.running && SDL_WaitEvent(&event)) {
		handle_event(&state, &event);
		if (state.redraw_needed) {
			redraw(&state);
			state.redraw_needed = 0;
		}
	}

	ui_destroy(&state);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	return 0;
}
