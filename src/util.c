
Vector tvtw(struct State *state, Vector v) {
	Vector wp = state->viewport_pos;
	Vector ws = state->window_size;

	return (Vector){
		(int)ceil((double)ws.x * 0.5 - (double)v.x * state->zoom - (double)wp.x * state->zoom),
		(int)ceil((double)ws.y * 0.5 - (double)v.y * state->zoom - (double)wp.y * state->zoom)
	};
}

Vector tvti(struct State *state, Vector v) {
	Vector wp = state->viewport_pos;
	Vector ws = state->window_size;

	return (Vector){
		(int)ceil(((double)ws.x * 0.5 - (double)v.x - (double)wp.x * state->zoom) / state->zoom),
		(int)ceil(((double)ws.y * 0.5 - (double)v.y - (double)wp.y * state->zoom) / state->zoom)
	};
}

int min2(int a, int b) {
	if (a < b) {
		return a;
	}
	return b;
}

int max2(int a, int b) {
	if (a > b) {
		return a;
	}
	return b;
}

int clamp(int a, int min, int max) {
	if (a < min) {
		return min;
	} else if (a > max) {
		return max;
	}
	return a;
}
