CC=gcc
SRC=src/main.c src/event.c src/ui.c src/input.c src/logic.c src/util.c src/state.c

imgcell: $(SRC)
	$(CC) -I src $(shell sdl2-config --cflags) src/main.c $(shell sdl2-config --libs) -lSDL2_image -lm -o imgcell

install: imgcell
	install -D imgcell /usr/local/bin/imgcell
